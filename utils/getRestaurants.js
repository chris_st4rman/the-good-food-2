import axios from 'axios'

export async function getRestaurants(page) {
  let data = { restaurants: "", data: "" }
  let params = { page: page }
  await axios
    .get('/api/restaurants', {
      proxy: {
        host: process.env.GOODFOOD_API_URL,
      },
    }, { params: params })
    .then((result) => {
      data.restaurants = result.data.content
    })
  await axios.get(
    '/api/restaurants/count', {
    proxy: {
      host: process.env.GOODFOOD_API_URL,
    },
  }).then((result) => {
    data.count = result.data
  })
  return data
}
