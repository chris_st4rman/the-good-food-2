import axios from 'axios'

export async function getRestaurant(restaurantId) {
  let data
  await axios
    .get(`/api/restaurant/${restaurantId}`, {
      proxy: {
        host: process.env.GOODFOOD_API_URL,
      },
    })
    .then((result) => {
      data = result.data
    })
  return data
}
