import axios from 'axios'

export async function getUsersByLevel(level, token) {
  let data
  await axios
    .get('/api/getUsersByLevel', {
      params: {
        level: level,
      },
      proxy: {
        host: process.env.GOODFOOD_API_URL,
      },
      headers: {
        Authorization: token,
      },
    })
    .then((result) => {
      data = result.data
    })
  return data
}
