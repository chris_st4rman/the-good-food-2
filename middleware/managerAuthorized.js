export default function ({ app, error }) {
  // If the user is not an admin
  if (!app.$auth.user || app.$auth.user.level !== 'Manager') {
    return error({
      statusCode: 404,
      message: "Cette page n'existe pas",
    })
  }
}
