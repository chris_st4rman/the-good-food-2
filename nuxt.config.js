import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'The Good Food',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Sur place, à emporter ou en livraison, mangez bien, mangez Good Food !',
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.png',
      },
    ],
  },

  // router: {
  //   middleware: ['auth'],
  // },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ['@nuxtjs/axios', '@nuxtjs/auth-next', '@nuxtjs/proxy'],

  axios: {
    proxy: true,
    prefix: '/api/',
  },

  proxy: {
    '/api': process.env.GOODFOOD_API_URL || 'http://api/',
  },

  auth: {
    strategies: {
      local: {
        token: {
          property: 'token',
          global: true,
          // required: true,
          type: false,
        },
        user: {
          property: '',
        },
        endpoints: {
          login: { url: '/auth/signin', method: 'post' },
          logout: {
            url: '/user/logout',
            method: 'post',
          },
          user: { url: '/user', method: 'get' },
        },
      },
    },
    auth: {
      redirect: {
        login: '/login',
        logout: '/',
        callback: '/login',
        home: '/',
      },
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
  },

  env: {
    GOODFOOD_API_URL: process.env.GOODFOOD_API_URL,
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
