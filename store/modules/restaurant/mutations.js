import * as types from './mutation_types'

export default {
  [types.UPDATE_RESTAURANT](state, restaurant) {
    state.restaurant = restaurant
  },
}
