export default function ({ app, error }) {
  // If the user is not a manager
  if (!app.$auth.user || app.$auth.user.level !== 'Admin') {
    return error({
      statusCode: 404,
      message: "Cette page n'existe pas",
    })
  }
}
