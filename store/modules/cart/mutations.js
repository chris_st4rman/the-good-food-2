import * as types from './mutation_types'

export default {
  [types.EMPTY_CART](state) {
    Object.assign(state, { orderItems: [] })
  },

  [types.PLUS_QUANTITY](state, itemToAdd) {
    let index = null

    state.orderItems.forEach((orderItem, stateIndex) => {
      if (orderItem.item.id == itemToAdd.id) {
        index = stateIndex
      }
    })

    if (index !== null) {
      state.orderItems[index].quantity++
    } else {
      state.orderItems.push({ item: itemToAdd, quantity: 1 })
    }
  },

  [types.MINUS_QUANTITY](state, item) {
    let index = null
    state.orderItems.forEach((orderItem, stateIndex) => {
      if (orderItem.item.id === item.id) {
        index = stateIndex
      }
    })
    if (index !== null) {
      if (state.orderItems[index].quantity === 1) {
        state.orderItems.splice(index, 1)
      } else {
        state.orderItems[index].quantity--
      }
    }
  },

  [types.UPDATE_TOTAL](state) {
    state.totalItems = state.orderItems.length
  },

  [types.UPDATE_TOTAL_PRICE](state) {
    let totalPrice = 0
    state.orderItems.forEach((orderItem) => {
      totalPrice = totalPrice + orderItem.item.price * orderItem.quantity
    })
    state.totalPrice = totalPrice
  },

  [types.UPDATE_RESTAURANT](state, restaurantId) {
    state.restaurantId = restaurantId
  },
}
