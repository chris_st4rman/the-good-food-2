import dayjs from 'dayjs'

const dateFormat = 'DD-MM-YYYY'
export default function stringToDate(stringDate) {
  const date = new Date(stringDate)
  return date
}
