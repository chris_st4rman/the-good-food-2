import axios from 'axios'

export async function getUserById(userId, token) {
  let data
  await axios
    .get(`/api/user/${userId}`, {
      proxy: {
        host: process.env.GOODFOOD_API_URL,
      },
      headers: {
        Authorization: token,
      },
    })
    .then((result) => {
      data = result.data
    })
  return data
}
