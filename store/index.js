import { getField, updateField } from 'vuex-map-fields'
import cart from './modules/cart'
import restaurant from './modules/restaurant'

export default {
  namespaced: true,
  modules: {
    cart,
    restaurant,
  },
  mutations: {
    updateField,
  },
  getters: {
    getField,
  },
}
