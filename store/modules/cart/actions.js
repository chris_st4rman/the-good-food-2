import * as types from './mutation_types'

export const plusQuantity = function ({ commit }, item) {
  commit(types.PLUS_QUANTITY, item)
  commit(types.UPDATE_TOTAL_PRICE)
}

export const minusQuantity = function ({ commit }, item) {
  commit(types.MINUS_QUANTITY, item)
  commit(types.UPDATE_TOTAL_PRICE)
}

export const emptyCart = function ({ commit }) {
  commit(types.EMPTY_CART)
  commit(types.UPDATE_TOTAL_PRICE)
}

export const updateRestaurant = function ({ commit }, restaurantId) {
  commit(types.UPDATE_RESTAURANT, restaurantId)
}
