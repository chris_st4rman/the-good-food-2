import * as types from './mutation_types'

export const updateRestaurant = function ({ commit }, restaurant) {
  commit(types.UPDATE_RESTAURANT, restaurant)
}
